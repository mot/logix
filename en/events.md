---
title: Events
---

# Events
## Upcoming:
     - card10 meetup in Berlin: xHain 05.07. 19:00

## Previous
     - CCCamp 2019 Badge at #eh19 (easterhegg2019): [Description](https://conference.c3w.at/eh19/talk/DA7KTT/), [Recording](https://media.ccc.de/search/?q=CCCamp+Badge+Talk)
     - CCCamp2019 Badge Workshop at #eh19: [Description](https://conference.c3w.at/eh19/talk/VHFMJ7/), 2019-04-21, 14:30–15:30
     - card10 at the [GPN19](https://entropia.de/GPN19:There_will_have_been_a_camp_badge:_We%27re_reconstructing_future_technology_and_you_can_help_us_with_this_mission)
     - card10 meetup in Berlin: xHain 14.06. 19:00
